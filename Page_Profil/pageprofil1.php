<!doctype html>
<html lang="fr">

<head>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"><!-- Required meta tags -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
        crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="pageprofil.css">

    <!------ Include the above in your HEAD tag ---------->

    <title>Instatram</title>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-6">

                <div class="card hovercard bord">
                    <div class="cardheader">

                    </div>
                    <div class="avatar">
                        <img alt="" src="profil2.jpeg">
                    </div>
                    <div class="info">
                        <div class="title">
                            <p>Sarah Fréchit</p>
                        </div>
                        <div class="desc">Professionnelle dentaire </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
    <!--galerie-->
    <section class="pad">

        <div class="container col-12">
            <div class="row pic2 try">
                <div class="col-4">
                <a href="#modal" data-toggle="modal" class="photoModal"id="6">
                        <img src="image6.jpeg" width="400" heigth="300" alt="Souvenir d'Australie " />
                    </a>
                </div>

                <div class="col-4">
                <a href="#modal" data-toggle="modal" class="photoModal" id="7">
                        <img src="image7.jpeg" width="400" heigth="300" alt="Le gros chat" />
                    </a>
                </div>

                <div class="col-4">
                <a href="#modal" data-toggle="modal" class="photoModal" id="8">
                        <img src="image8.jpeg" width="400" heigth="300" alt="Ma main dans ta gueule !" />
                    </a>
                </div>
            </div>
            <div class="row pic2">
                <div class="col-4 offset-2">
                <a href="#modal" data-toggle="modal" class="photoModal" id="9">
                        <img src="image9.jpeg" width="400" heigth="300" alt="La fleur éphémère" />
                    </a>
                </div>

                <div class="col-3">
                <a href="#modal" data-toggle="modal" class="photoModal" id="10">
                        <img src="image10.jpeg" width="400" heigth="300" alt="Pâtisserie" />
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"id="contentModal">
                        <img src="image1.jpeg" width="400" heigth="300" alt="Image 1" data-toggle="modal" data-target="#modal" />
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"><i class="fas fa-heart"></i></button>
                        <button type="button" class="btn btn-primary"><i class="fas fa-comments"></i></button>
                    </div>
                </div>
            </div>
        </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
        <script type="text/javascript" src="insta.js"></script>
</body>

</html>