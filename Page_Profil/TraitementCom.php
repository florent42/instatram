<?php

// Connexion à la base de données
try {
    $bdd = new PDO('mysql:host='.(getenv('MYSQL_HOST') ?: 'localhost').';dbname=Instagram;charset=utf8', 'root', '');  
}

catch(Exception $e) {
        die('Erreur : '.$e->getMessage());
}

// enregistrement des commentaire en bdd
$photos = $bdd->prepare('INSERT INTO Photo (commentaire, id_profil) VALUE(?, ?)');
$photos ->execute(array($_POST['commentaire'], $_POST['id_photo']));
