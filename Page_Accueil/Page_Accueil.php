<!doctype html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="styleAccueil.css">

    <title>Instatram</title>
</head>

<body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand bouttonNav" href="http://localhost/Projet_Instagram/Page_Accueil.php">Accueil</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>               
            <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item active">
                      <a class="nav-link" href="../Page3profil/Page3Profil.php">Profil<span class="sr-only">(current)</span></a>
                    </li>                    
                  </ul>
                <form class="form-inline recherche">
                    <input class="form-control mr-sm-2" type="search" placeholder="non fonctionnel" aria-label="Search">
                    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Recherche</button>
                </form>
            </div>
        </nav>
    <div class="placeLogo">
        <img src='logo.gif'>
    </div>

    <div class="container">
        <div class="row">
            <div class="input-group col-6 offset-4 impu mb-3"><!-- ../Page3profil/Page3Profil.php -->
                <form action="../TraitementCookie.php" method="post" class="form-inline" >
                    <input type="text" class="form-control" placeholder="Pseudo" name="Pseudo" aria-label="Recipient's username" aria-describedby="button-addon2">
                    <div class="input-group-append" href="http://localhost/Projet_Instagram/Page_profil/PageProfil.php">
                        <button class="btn btn-danger" type="submit" id="button-addon2">Go..</button>
                    </div>  
                </form>
            </div>
        </div>
    </div>

    <nav class="navbar fixed-bottom navbar-dark bg-dark">
        <a class="navbar-brand"></a>
    </nav>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="insta.js"></script>    

</body>
</html>